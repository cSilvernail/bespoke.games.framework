#include "pch.h"
#include "PerlinNoiseGame.h"

using namespace std;
using namespace DirectX;
using namespace Library;

namespace PerlinNoiseDemo
{
	const XMVECTORF32 PerlinNoiseGame::BackgroundColor = Colors::CornflowerBlue;

	PerlinNoiseGame::PerlinNoiseGame(std::function<void*()> getWindowCallback, std::function<void(SIZE&)> getRenderTargetSizeCallback) :
		Game(getWindowCallback, getRenderTargetSizeCallback), mRenderStateHelper(*this)
	{
	}

	void PerlinNoiseGame::Initialize()
	{
		SamplerStates::Initialize(mDirect3DDevice.Get());
		BlendStates::Initialize(mDirect3DDevice.Get());
		RasterizerStates::Initialize(mDirect3DDevice.Get());
		DepthStencilStates::Initialize(mDirect3DDevice.Get());
		SpriteManager::Initialize(*this);

		const float viewWidth = 5.0f;
		const float viewHeight = 5.0f;
		auto camera = make_shared<OrthographicCamera>(*this, viewWidth, viewHeight);
		mComponents.push_back(camera);
		mServices.AddService(Camera::TypeIdClass(), camera.get());

		mKeyboard = make_shared<KeyboardComponent>(*this);
		mComponents.push_back(mKeyboard);
		mServices.AddService(KeyboardComponent::TypeIdClass(), mKeyboard.get());

		mMouse = make_shared<MouseComponent>(*this, MouseModes::Relative);
		mComponents.push_back(mMouse);
		mServices.AddService(MouseComponent::TypeIdClass(), mMouse.get());

		mGamePad = make_shared<GamePadComponent>(*this);
		mComponents.push_back(mGamePad);
		mServices.AddService(GamePadComponent::TypeIdClass(), mGamePad.get());

		mFpsComponent = make_shared<FpsComponent>(*this);
		mFpsComponent->Initialize();

		mDirect3DDeviceContext->OMSetDepthStencilState(DepthStencilStates::NoDepthCulling.Get(), 0);

		PerlinNoise::CreateInstance();
		const uint32_t width = 128;
		const uint32_t height = 128;
		const uint32_t octave = 5;
		const auto noise = PerlinNoise::GeneratePerlinNoise(width, height, octave);

		Microsoft::WRL::ComPtr<ID3D11Texture2D> texture;
		auto noiseSRV = PerlinNoise::CreateTextureFromNoise(mDirect3DDevice.Get(), noise, texture.ReleaseAndGetAddressOf());
		auto noiseTexture = make_shared<Texture2D>(noiseSRV, width, height);

		mPerlinNoiseSprite = make_shared<Sprite>(*this, camera, noiseTexture);
		mComponents.push_back(mPerlinNoiseSprite);
		mContentManager.AddAsset(Utility::ToWideString(mPerlinNoiseSprite->Name()), mPerlinNoiseSprite);
		PerlinNoise::Shutdown();

		Game::Initialize();
		camera->SetPosition(0.0f, 0.0f, 1.0f);
	}

	void PerlinNoiseGame::Update(const GameTime &gameTime)
	{
		if (mKeyboard->WasKeyPressedThisFrame(Keys::Escape) || mGamePad->WasButtonPressedThisFrame(GamePadButtons::Back))
		{
			Exit();
		}

		mFpsComponent->Update(gameTime);

		Game::Update(gameTime);
	}

	void PerlinNoiseGame::Draw(const GameTime &gameTime)
	{
		mDirect3DDeviceContext->ClearRenderTargetView(mRenderTargetView.Get(), reinterpret_cast<const float*>(&BackgroundColor));
		mDirect3DDeviceContext->ClearDepthStencilView(mDepthStencilView.Get(), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
		
		Game::Draw(gameTime);

		mRenderStateHelper.SaveAll();
		mFpsComponent->Draw(gameTime);
		mRenderStateHelper.RestoreAll();

		HRESULT hr = mSwapChain->Present(1, 0);

		// If the device was removed either by a disconnection or a driver upgrade, we must recreate all device resources.
		if (hr == DXGI_ERROR_DEVICE_REMOVED || hr == DXGI_ERROR_DEVICE_RESET)
		{
			HandleDeviceLost();
		}
		else
		{
			ThrowIfFailed(hr, "IDXGISwapChain::Present() failed.");
		}
	}

	void PerlinNoiseGame::Shutdown()
	{
		SpriteManager::Shutdown();
		RasterizerStates::Shutdown();
		BlendStates::Shutdown();
		SamplerStates::Shutdown();
		DepthStencilStates::Shutdown();

		Game::Shutdown();
	}

	void PerlinNoiseGame::Exit()
	{
		PostQuitMessage(0);
	}
}