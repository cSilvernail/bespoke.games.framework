#pragma once

#include "DrawableGameComponent.h"
#include "SpriteMaterial.h"
#include "Transform2D.h"

namespace Library
{
	class Texture2D;

	class Sprite : public DrawableGameComponent
	{
		RTTI_DECLARATIONS(Sprite, DrawableGameComponent)

	public:
		Sprite(Game& game, const std::shared_ptr<Camera>& camera, const std::shared_ptr<Texture2D>& texture, const Microsoft::WRL::ComPtr<ID3D11SamplerState>& samplerState = SamplerStates::TrilinearClamp, std::uint32_t horizontalTileCount = SpriteMaterial::DefaultHorizontalTileCount, std::uint32_t verticalTileCount = SpriteMaterial::DefaultVerticalTileCount);
		Sprite(const Sprite&) = default;
		Sprite& operator=(const Sprite&) = default;
		Sprite(Sprite&&) = default;
		Sprite& operator=(Sprite&&) = default;
		virtual ~Sprite() = default;

		const std::string& Name() const;
		void SetName(const std::string& name);

		std::shared_ptr<SpriteMaterial> Material() const;
		void SetMaterial(const std::shared_ptr<SpriteMaterial>& material);

		const Transform2D& GetTransform() const;
		void SetTransform(const Transform2D& transform);
		
		void ApplyRotation(float angle);

		virtual void Initialize() override;
		virtual void Draw(const GameTime& gameTime) override;

	protected:
		void UpdateMaterial();

		std::string mName;
		std::shared_ptr<SpriteMaterial> mMaterial;
		Transform2D mTransform;
	};
}