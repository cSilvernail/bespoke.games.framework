#include "GameObject.h"
namespace Library
{
	inline const Transform& GameObject::GetTransform() const
	{
		return mTransform;
	}

	inline void GameObject::SetTransform(const Transform& transform)
	{
		mTransform = transform;
	}

	inline const std::string& GameObject::Tag() const
	{
		return mTag;
	}

	inline void GameObject::SetTag(const std::string& tag)
	{
		mTag = tag;
	}

	inline const std::map<std::uint64_t, std::vector<std::shared_ptr<GameComponent>>>& GameObject::Components() const
	{
		return mComponents;
	}
}